var gulp = require('gulp');
var concat = require('gulp-concat');
var debug = require('gulp-debug');
var config = require('../config');

var isLocal = config.env.isLocal;
var isProduction = config.env.isProduction;
var paths = isLocal ? config.localPaths : config.paths;

gulp.task('concat:jade', function() {
  return gulp.src(paths.blocks + '**/*.jade')
    .pipe(concat('blocks.jade'))
    .pipe(gulp.dest(paths.tmp));
});

gulp.task('concat:js', function() {
  return gulp.src([paths.localjs + '**/*.jade'])
    .pipe(concat('vendor-js.jade'))
    .pipe(gulp.dest(paths.tmp));
});

gulp.task('concat:otherJs', function() {
  var targets = [paths.localjs + '**/*.js'];
  targets.push(paths.localjs + '**/*.css');
  targets.push(paths.localjs + '**/*.png');
  if(isProduction) {
    targets.push('!' + paths.localjs +'**/*.dummy.js');
    targets.push('!' + paths.localjs +'**/tech.js');
  }
  return gulp.src(targets)
    .pipe(gulp.dest(paths.js));
});

gulp.task('concat:blockJs', function() {
	var targets = [paths.blocks + '**/*.js', paths.helpers+'blockTrigger.js'];
	 targets.push('!' + paths.blocks +'**/*.dummy.js');

  if(!isProduction) gulp.start('concat:blockDummyJs');

  return gulp.src(targets)
    .pipe(concat('blocks.js'))
    .pipe(gulp.dest(paths.js));
});

gulp.task('concat:blockDummyJs', function() {
  var targets = [paths.blocks +'**/*.dummy.js'];

  return gulp.src(targets)
      .pipe(concat('blocks.dummy.js'))
      .pipe(gulp.dest(config.localPaths.js));
});

gulp.task('concat:fonts', function() {
  return gulp.src([paths.fonts + '**/*.scss'])
    .pipe(concat('fonts.scss'))
    .pipe(gulp.dest(paths.tmp));
});

gulp.task('concat:scss', function() {
  return gulp.src([paths.blocks + '**/*.scss'])
    .pipe(concat('_blocks.scss'))
    .pipe(gulp.dest(paths.tmp));
});

gulp.task('concat:copyimg', function() {
  return gulp.src([paths.localimg + '**/*.*'])
    .pipe(gulp.dest(paths.img));
});

gulp.task('concat:copyfonts', function() {
  return gulp.src([paths.localfonts + '**/*.*'])
    .pipe(gulp.dest(paths.fonts));
});

gulp.task('concat', ['concat:jade', 'concat:blockJs', 'concat:otherJs', 'concat:js', 'concat:fonts', 'concat:scss', 'concat:copyimg', 'concat:copyfonts']);
