// Пиши скрипты сюда
$(document).ready(function() {
    
    //
    // Menu toggle
    $(".nav-toggle").on("click", function() {
        $(this).toggleClass('nav-toggle__line_active');
        $('.header-mobile__menu').toggleClass('hidden');
        $('.header-mobile').siblings().toggleClass('hidden');
    });

    //
    // Menu listing
    $(function() {
        $( '#dl-menu' ).dlmenu({
            animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
        });
    });
});